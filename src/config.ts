export const config = {
  PARSER_API: process.env.NEXT_PUBLIC_PARSER_API as string,
  ALLOWED_FILE_TYPES: ["application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"],
  SERVICE_NOW_API: process.env.SERVICE_NOW_API as string,
  SERVICE_NOW_USER: process.env.SERVICE_NOW_USER as string,
  SERVICE_NOW_PASSWORD: process.env.SERVICE_NOW_PASSWORD as string,
}
