import { config } from "@/config"
import { clsx, type ClassValue } from "clsx"
import { twMerge } from "tailwind-merge"

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}

export function getAuthHeader() {
  const { SERVICE_NOW_USER, SERVICE_NOW_PASSWORD } = config
  return 'Basic ' + Buffer.from(`${SERVICE_NOW_USER}:${SERVICE_NOW_PASSWORD}`, 'binary').toString('base64')
}

