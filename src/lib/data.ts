export const locations = [
  { value: 'chennai', label: 'Chennai', },
  { value: 'delhi', label: 'Delhi', },
  { value: 'gurgaon', label: 'Gurgaon', },
  { value: 'faridabad', label: 'Faridabad' },
  { value: 'noida', label: 'Noida' },
  { value: 'pune', label: 'Pune' },
  { value: 'ahmedabad', label: 'Ahmedabad' },
  { value: 'goa', label: 'Goa' },
  { value: 'nepal', label: 'Nepal' },
  { value: 'bhutan', label: 'Bhutan' },
]

export const genders = [
  { value: 'male', label: 'Male' },
  { value: 'female', label: 'Female' },
]

export const jobGrades = [
  { value: 'W01', label: 'W01', },
  { value: 'W02', label: 'W02', },
  { value: 'W03', label: 'W03', },
  { value: 'S01', label: 'S01' },
  { value: 'S02', label: 'S02' },
  { value: 'S03', label: 'S03' },
  { value: 'M01', label: 'M01' },
  { value: 'M02', label: 'M02' },
  { value: 'M03', label: 'M03' },
  { value: 'M04', label: 'M04' },
  { value: 'M05', label: 'M05' },
  { value: 'M06', label: 'M06' },
  { value: 'M07', label: 'M07' },
  { value: 'M08', label: 'M08' },
  { value: 'M09', label: 'M09' },
]

export const colsLabelMap: Record<string, any> = {
  name: "Name",
  job_grade: "Job Grade",
  location: "Location",
  gender: "Gender",
}

export const cols = [
  "name",
  "job_grade",
  "location",
  "gender",
]

export const ticketCols = [
  "id",
  "name",
  "status",
  "type",
]
