import sqlite from 'sqlite3'

export let db = initDB()

function initDB() {
  const db = new sqlite.Database('vb.db', sqlite.OPEN_READWRITE, (err) => {
    if (err) {
      console.error("error: failed to connect to db")
    }
    console.log('connected to db')
  })
  db.get("PRAGMA foreign_keys = ON")
  return db
}

export function queryDB(query: string, params: string[]): Promise<unknown[]> {
  return new Promise((res, rej) => {
    db.all(query, params, (err, rows) => {
      if (err) {
        rej(err)
      }
      res(rows)
    })
  })
}

export function runQuery(query: string, params: string[]): Promise<number> {
  return new Promise((res, rej) => {
    db.run(query, params, function(err) {
      if (err) {
        rej(err)
      }
      res(this.lastID)
    })
  })
}
