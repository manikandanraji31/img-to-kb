'use server'

export async function createUserAction(user: Record<string, null>) {
  // setLoading(true)
  try {
    const res = await fetch("/api/users", {
      method: "POST",
      body: JSON.stringify(user)
    })
    if (!res.ok) {
      throw new Error("failed to create new user")
    }
    // setOpen(false)
    // revalidatePath('/', 'layout')
  } catch (err) {
    console.error("err:", err)
    // toast.error("Failed to create new user")
  } finally {
    // setLoading(false)
  }
}

