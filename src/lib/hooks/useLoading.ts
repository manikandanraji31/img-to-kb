import { useState } from "react";

export function useLoading(initialValue: boolean, delay = 300) {
  const [loading, _setLoading] = useState(initialValue)

  function setLoading(loading: boolean) {
    setTimeout(() => {
      _setLoading(loading)
    }, loading ? 0 : delay)
  }
  
  return [loading, setLoading] as const
}
