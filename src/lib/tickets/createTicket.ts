type Ticket = {
  name: string
  user: string
  type: string
}
import { config } from '../../config'
import { getAuthHeader } from '../utils'

export const ticketStatusMap = {
  "1": "New",
  "2": "In Progress",
  "3": "On Hold",
  "6": "Resolved",
  "7": "Closed",
  "8": "Cancelled",
}

// ticket format: <name>,<user>,<type>
export async function createTicket(ticket: Ticket) {
  const { name, user, type } = ticket
  const { SERVICE_NOW_API } = config
  const path = SERVICE_NOW_API + '/now/table/incident?sysparm_fields=description,state,sys_id,short_description'
  try {
    const res = await fetch(path, {
      method: "POST",
      body: JSON.stringify({
        description: name,
        short_description: [user, type].join(','),
        state: "1",
        caller_id: "62826bf03710200044e0bfc8bcbe5df1",
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': getAuthHeader(),
      }
    })
    if (!res.ok) {
      throw new Error('failed to create ticket')
    }
    return { ...ticket, status: ticketStatusMap["1"] }
  } catch (err) {
    console.error("error: createTicket:", err)
    throw new Error('failed to create ticket')
  }
}
