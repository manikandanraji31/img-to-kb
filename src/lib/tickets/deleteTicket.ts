import { config } from '../../config'
import { getAuthHeader } from '../utils'

export async function deleteTicket(ticketId: string) {
  const { SERVICE_NOW_API } = config
  const path = SERVICE_NOW_API + `/now/table/incident/${ticketId}`
  try {
    const res = await fetch(path, {
      method: "DELETE",
      headers: {
        'Authorization': getAuthHeader(),
      }
    })
    if (!res.ok) {
      throw new Error('failed to delete ticket')
    }
    return true
  } catch (err) {
    console.error("error: deleteTicket:", err)
    throw new Error('failed to delete ticket')
  }
}

