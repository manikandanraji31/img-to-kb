import { config } from "@/config";
import { getAuthHeader } from "../utils";
import { ticketStatusMap } from "./createTicket";

export async function getTickets(username: string, type?: string) {
  const path = config.SERVICE_NOW_API + '/now/table/incident?sysparm_query=short_descriptionLIKE_policy&sysparm_limit=100&sysparm_fields=description,sys_id,state,short_description'
  try {
    const res = await fetch(path, {
      headers: {
        'Authorization': getAuthHeader()
      }
    })
    if (!res.ok) {
      throw new Error('failed to get tickets')
    }
    const { result } = await res.json()
    const tickets: any[] = []
    result.forEach((item: any) => {
      const tokens = item.short_description.split(",")
      if (tokens.length === 2) {
        const [user, docType] = tokens
        if (username === user && (!type ? true : docType === type)) {
          tickets.push({
            id: item.sys_id,
            name: item.description,
            status: (ticketStatusMap as any)[item.state],
            user,
            type: docType
          })
        }
      }
    })
    return tickets
  } catch (err) {
    console.error('error: getTickets:', err)
    throw new Error('failed to get tickets')
  }
}
