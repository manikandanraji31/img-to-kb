import { queryDB, runQuery } from "@/lib/db/index";
import { deleteTicket } from "@/lib/tickets/deleteTicket";
import { getTickets } from "@/lib/tickets/getTickets";
import { NextRequest, NextResponse } from "next/server";

type Request = {
  params: {
    id: string
  }
}

export async function DELETE(req: NextRequest, { params }: Request) {
  const url = new URL(req.url)
  const isExternal = url.searchParams.get("external")
  try {
    if (isExternal) {
      await deleteTicket(params.id)
    } else {
      await runQuery('DELETE FROM tickets where id = ?', [params.id])
    }
    return NextResponse.json({
      message: 'Ticket deleted successfully'
    })
  } catch (err) {
    return NextResponse.json(
      { message: "internal server error" },
      {
        status: 500,
      },
    );
  }
}

export async function GET(req: NextRequest, { params }: Request) {
  const url = new URL(req.url)
  const isExternal = url.searchParams.get("external")
  try {
    let tickets: any[]
    if (isExternal) {
      tickets = await getTickets(params.id)
    } else {
      tickets = await queryDB('SELECT id, name, type, status FROM tickets WHERE user_id = ?', [params.id])
    }
    return NextResponse.json({
      tickets
    })
  } catch (err) {
    return NextResponse.json(
      { message: "internal server error" },
      {
        status: 500,
      },
    );
  }
}

