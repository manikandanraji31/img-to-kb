import { queryDB, runQuery } from "@/lib/db/index"
import { createTicket } from "@/lib/tickets/createTicket"
import { getTickets } from "@/lib/tickets/getTickets"
import { headers } from "next/headers"
import { NextRequest, NextResponse } from "next/server"
import { z } from 'zod'

const newTicketSchema = z.object({
  name: z.string(),
  type: z.union([z.literal("leave_policy"), z.literal("travel_policy")]),
})

export async function GET(request: NextRequest) {
  const url = new URL(request.url)
  const type = url.searchParams.get("type")
  const username = url.searchParams.get("user")
  if (!type || !username) {
    return NextResponse.json({
      message: 'bad request'
    }, { status: 401 })
  }
  try {
    const isExternal = url.searchParams.get("external")
    if (isExternal) {
      const tickets = await getTickets(username, type)
      return NextResponse.json({
        tickets
      })
    }
    const tickets = await queryDB("SELECT t.* FROM tickets t LEFT JOIN users u on u.id = t.user_id WHERE u.name = ? AND t.type = ?", [
      username,
      type,
    ])
    return NextResponse.json({
      tickets
    })
  } catch (err) {
    return NextResponse.json({
      message: 'internal server error'
    }, { status: 500 })
  }
}

export async function POST(request: NextRequest) {
  const userId = headers().get("x-user-id")
  const body = await request.json()
  const parsedBody = newTicketSchema.safeParse(body)
  if (!parsedBody.success) {
    return NextResponse.json({
      message: 'bad request'
    }, { status: 400 })
  }
  const data = parsedBody.data
  try {
    const url = new URL(request.url)
    const isExternal = url.searchParams.get("external")
    if (isExternal) {
      const ticket = await createTicket({
        type: data.type,
        name: data.name,
        user: headers().get("x-user-name") ?? ''
      })
      return NextResponse.json({
        message: 'Ticket created successfully',
        ticket
      })
    }
    const ticketId = await runQuery("INSERT INTO tickets(name, type, status, user_id) VALUES(?, ?, ?, ?)", [
      data.name,
      data.type,
      'TODO',
      userId ?? '',
    ])
    const [ticket] = await queryDB('SELECT id, name, type, status FROM tickets where id = ?', [String(ticketId)])
    return NextResponse.json({
      message: 'Ticket created successfully',
      ticket
    })
  } catch (err) {
    console.error("error: POST /tickets:", err)
    return NextResponse.json({
      message: 'internal server error'
    }, { status: 500 })
  }
}
