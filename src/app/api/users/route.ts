import { queryDB, runQuery } from "@/lib/db/index";
import { NextRequest, NextResponse } from "next/server";
import { z } from 'zod'

const newUserSchema = z.object({
  jobGrade: z.string(),
  name: z.string(),
  location: z.string(),
  password: z.string(),
  gender: z.string(),
})

export async function GET() {
  const users = await queryDB("SELECT * FROM users", [])
  return NextResponse.json({ users })
}

export async function POST(request: NextRequest) {
  const body = await request.json()
  const parsedBody = newUserSchema.safeParse(body)
  if (!parsedBody.success) {
    return NextResponse.json({
      message: 'bad request'
    }, { status: 400 })
  }
  const data = parsedBody.data

  try {
    await runQuery("INSERT INTO users(name, job_grade, location, password, gender) VALUES(?, ?, ?, ?, ?)", [
      data.name,
      data.jobGrade,
      data.location,
      data.password,
      data.gender,
    ])
    return NextResponse.json({
      message: 'User created successfully'
    })
  } catch (err) {
    console.error("error: POST /users:", err)
    return NextResponse.json({
      message: 'internal server error'
    }, { status: 500 })
  }
}
