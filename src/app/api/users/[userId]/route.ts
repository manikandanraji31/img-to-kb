import { runQuery } from "@/lib/db/index";
import { NextRequest, NextResponse } from "next/server";

type Request = {
  params: {
    userId: string
  }
}

export async function DELETE(_req: NextRequest, { params }: Request) {
  try {
    await runQuery('DELETE FROM users where id = ?', [params.userId])
    return NextResponse.json({
      message: 'User delete successfully'
    })
  } catch (err) {
    return NextResponse.json(
      { message: "internal server error" },
      {
        status: 500,
      },
    );
  }
}

