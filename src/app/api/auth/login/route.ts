import { queryDB } from "@/lib/db/index";
import { NextRequest, NextResponse } from "next/server";
import { z } from 'zod'

const loginSchema = z.object({
  username: z.string(),
  password: z.string(),
})

export async function POST(req: NextRequest) {
  const body = await req.json()
  const parsedBody = loginSchema.safeParse(body)
  if (!parsedBody.success) {
    return NextResponse.json({
      message: 'bad request'
    }, { status: 400 })
  }
  const data = parsedBody.data
  try {
    const rows = await queryDB('SELECT * FROM users WHERE name = ? AND password = ?', [data.username, data.password])
    if (rows.length === 0) {
      return NextResponse.json({
        message: 'bad request'
      }, { status: 400 })
    }
    const [user] = rows
    return NextResponse.json({
      token: btoa(JSON.stringify(user))
    })
  } catch (err) {
    console.error("error: /auth/login:", err)
    return NextResponse.json({
      message: 'internal server error'
    }, { status: 500 })
  }
}
