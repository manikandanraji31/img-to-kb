export const functions = [
	{
		name: "getTickets",
		description: "Get the tickets for the user based on the document.",
		parameters: {
			type: "object",
			properties: {
				type: {
					type: "string",
					description: "The policy of the document that the user is interacting with. Possible values: leave_policy, travel_policy",
				},
				user: {
					type: "string",
					description: "The user who is logged in.",
				},
			},
			required: ["type", "user"],
		},
	},
	{
		name: "createTicket",
		description: "Create a ticket for the user based on the document",
		parameters: {
			type: "object",
			properties: {
				type: {
					type: "string",
					description: "The policy of the document that the user is interacting with. Possible values: leave_policy, travel_policy",
				},
				user: {
					type: "string",
					description: "The user who is logged in.",
				},
				name: {
					type: "string",
					description: "The name of the ticket that the user want to create",
				},
			},
			required: ["name", "type", "user"],
		},
	},
];
