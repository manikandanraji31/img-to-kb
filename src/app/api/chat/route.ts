import { NextRequest, NextResponse } from "next/server";
import OpenAI from "openai";
import { prompt } from "./prompt";
import { headers } from "next/headers";
import { functions } from "./functions";

export async function POST(req: NextRequest) {
  try {
    const headersList = headers();
    const user = {
      name: headersList.get("x-user-name"),
      jobGrade: headersList.get("x-user-job-grade"),
      location: headersList.get("x-user-location"),
      gender: headersList.get("x-user-gender"),
    }
    const openai = new OpenAI({
      apiKey: process.env.OPENAI_API_KEY,
    });
    const { messages, parserResponse } = await req.json();
    const responseStream = new TransformStream();
    const writer = responseStream.writable.getWriter();
    const encoder = new TextEncoder();
    const systemMessage = {
      role: "system",
      content: parserResponse.pdf
        ? prompt.pdf(parserResponse.pdf)
        : prompt.docx(parserResponse.docx, JSON.stringify(user)),
    };
    const stream = openai.beta.chat.completions.stream({
      model: "gpt-4",
      stream: true,
      functions,
      function_call: "auto",
      messages: [systemMessage, ...messages],
    });
    stream.on("content", (delta) => {
      writer.write(encoder.encode(delta));
    });
    stream.on("end", () => {
      stream.finalMessage().then((message) => {
        if ("function_call" in message) {
          writer.write(JSON.stringify(message));
        }
        writer.write("__EOF__");
      })
    });
    stream.on("error", (error) => {
      console.error("/api/chat:", error);
      writer.write("__ERROR__");
    });
    return new Response(responseStream.readable, {
      headers: {
        "Content-Type": "text/event-stream",
        "Cache-Control": "no-cache, no-transform",
      },
    });
  } catch (err) {
    console.error("error: /chat", err);
    return NextResponse.json(
      { message: "internal server error" },
      {
        status: 500,
      },
    );
  }
}
