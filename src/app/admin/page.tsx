'use client'

import { useEffect, useState } from 'react'
import { CreateUser } from "@/components/CreateUser"
import { cn } from "@/lib/utils"
import { cols, colsLabelMap, ticketCols } from '@/lib/data'
import DeleteIcon from "@/assets/delete.svg";
import Image from "next/image";
import toast from 'react-hot-toast'

function Admin() {
  const [users, setUsers] = useState<any[]>([])
  const [tickets, setTickets] = useState<any[]>([])
  const [username, setUsername] = useState<null | string>(null)

  async function fetchUsers() {
    const res = await fetch('/api/users')
    const { users } = await res.json()
    setUsers(users)
  }

  async function fetchTickets() {
    try {
      const res = await fetch(`/api/tickets/${username}?external=true`, {
        headers: {
          Authorization: localStorage.getItem("token") ?? ""
        }
      })
      if (!res.ok) {
        throw new Error("error")
      }
      const { tickets } = await res.json()
      setTickets(tickets)
    } catch (err) {
      toast.error("Failed to fetch tickes")
    }
  }


  async function deleteUser(name: string) {
    const res = await fetch(`/api/users/${name}`, {
      method: 'DELETE',
    })
    if (!res.ok) {
      toast.error("Failed to delete user")
      return
    }
    fetchUsers()
    if (name == username) {
      setUsername("")
      setTickets([])
    }
  }


  async function deleteTicket(ticketId: string) {
    const res = await fetch(`/api/tickets/${ticketId}?external=true`, {
      method: 'DELETE',
      headers: {
        Authorization: localStorage.getItem("token") ?? ""
      }
    })
    if (!res.ok) {
      toast.error("Failed to delete ticket")
      return
    }
    fetchTickets()
  }

  useEffect(() => {
    fetchUsers()
  }, [])

  useEffect(() => {
    if (username) {
      fetchTickets()
    } else {
      setTickets([])
    }
  }, [username])

  return (
    <main className="p-4 max-w-[900px] my-4 mx-auto text-[#374151]">
      <div className="flex items-center mb-6 justify-between">
        <h1 className="text-2xl font-semibold">Admin</h1>
        <CreateUser fetchUsers={fetchUsers} />
      </div>

      <div>
        <p className="text-xl font-semibold mb-4">Users</p>
        <table className="w-full border-collapse">
          <thead>
            <tr>
              {cols.map(el => <th key={el} className="text-left p-4 bg-[#F5F5F5] ">{colsLabelMap[el]}</th>)}
            </tr>
          </thead>
          <tbody>
            {users.map((user: any) => {
              return (
                <tr className="relative group" key={user.name} onClick={() => setUsername(user.name)}>
                  {cols.map((col: any, i: number) => {
                    return (
                      <>
                        <td key={col} className={cn("p-4 border-r border-b border-[#7090B033]", {
                          "border-r-0": i === 3
                        })}>{(user as any)[col]}</td>
                        {i === 3 && (
                          <button onClick={() => {
                            deleteUser(user.id)
                          }} className="invisible absolute right-2 top-5 group-hover:visible">
                            <Image
                              width={16}
                              height={16}
                              alt="delete document"
                              src={DeleteIcon}
                            />
                          </button>
                        )}
                      </>
                    )
                  })}
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>


      <div className="mt-8">
        <p className="text-xl font-semibold mb-4">Tickets {username && <span className="bg-green-100 text-green-500 text-sm py-1 px-2 rounded-full">user: {username}</span>}</p>
        <table className="w-full border-collapse">
          <thead>
            <tr>
              {ticketCols.map(el => <th key={el} className="w-auto text-left p-4 bg-[#F5F5F5] capitalize">{el}</th>)}
            </tr>
          </thead>
          <tbody>
            {tickets.map((ticket: any) => {
              return (
                <tr className="relative group" key={ticket.name}>
                  {ticketCols.map((col: any, i: number) => {
                    return (
                      <>
                        <td key={col} className={cn("p-4 border-r border-b border-[#7090B033]", {
                          "border-r-0": i === 3
                        })}>{(ticket as any)[col]}</td>
                        {i === 3 && (
                          <button onClick={() => {
                            deleteTicket(ticket.id)
                          }} className="invisible absolute right-2 top-5 group-hover:visible">
                            <Image
                              width={16}
                              height={16}
                              alt="delete document"
                              src={DeleteIcon}
                            />
                          </button>
                        )}
                      </>
                    )
                  })}
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </main>
  )
}

export default Admin
