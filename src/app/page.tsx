"use client";

import { Chat } from "@/components/Chat";
import { Header } from "@/components/Header";
import { Login } from "@/components/Login";
import { UploadDoc } from "@/components/UploadDoc";
import { useEffect, useRef, useState } from "react";
import toast, { Toaster } from "react-hot-toast";

export type Message = {
  role: "system" | "assistant" | "user" | "function";
  name?: string
  content: string;
};

export type OCRResponse = {
  text: string;
  table: string;
};

export type ParserResponse = {
  pdf?: OCRResponse
  docx?: string
}

export type UploadStatus = "idle" | "uploading" | "uploaded";

export default function Home() {
  const [username, setUsername] = useState('')
  const containerRef = useRef<HTMLDivElement>(null);
  const [open, setOpen] = useState(false)
  const [loading, setLoading] = useState(false)
  const [state, setState] = useState<{
    uploadStatus: UploadStatus;
    messages: Message[];
    parserResponse: ParserResponse
  }>({
    uploadStatus: "idle",
    messages: [],
    parserResponse: {},
  });

  function setUploadStatus(uploadStatus: UploadStatus, parserResponse: ParserResponse) {
    setState((prev) => ({
      ...prev,
      uploadStatus,
      parserResponse,
      messages: [],
    }));
  }

  function appendMessage(message: Message) {
    setState((prev) => ({
      ...prev,
      messages: [...prev.messages, message],
    }));
  }

  function setMessageAt(message: Message, index: number) {
    setState((prev) => ({
      ...prev,
      messages: prev.messages.map((msg, i) => (i === index ? message : msg)),
    }));
  }

  async function onNewMessage(value: string) {
    const message = {
      role: "user",
      content: value,
    } as const;
    appendMessage(message);
    streamBotResponse([...state.messages, message]);
  }

  function clearChat() {
    setState((prev) => ({
      ...prev,
      messages: [],
    }));
  }

  function logout() {
    setState({
      uploadStatus: 'idle',
      messages: [],
      parserResponse: {}
    })
    localStorage.removeItem("token")
    setUsername("")
    setOpen(true)
  }

  function login() {
    const token = localStorage.getItem('token')
    if (!token) {
      setOpen(true)
      return
    }
    try {
      const user = JSON.parse(atob(token))
      if (user.name) {
        setUsername(user.name)
        setOpen(false)
      }
    } catch (err) {
      console.error("invalid token")
      setOpen(true)
    }
  }

  function parseFnCall(response: string) {
    try {
      const data = JSON.parse(response)
      if ("function_call" in data) {
        return {
          ...data,
          function_call: {
            ...data.function_call,
            arguments: JSON.parse(data.function_call.arguments),
          }
        }
      }
    } catch (err) {
      return null
    }
  }

  async function invokeFnCall(fnCall: {
    name: string
    arguments: Record<string, any>
  }, messages: Message[]) {
    const functions: Record<string, Function> = {
      async getTickets(args: Record<string, any>) {
        setLoading(true)
        try {
          const { type, user } = args
          const res = await fetch(`/api/tickets?type=${type}&user=${user}&external=true`, {
            headers: {
              'Authorization': localStorage.getItem("token") ?? ""
            }
          })
          if (!res.ok) {
            throw new Error("error")
          }
          const data = await res.json()
          return data
        } catch (err) {
          return { tickets: [] }
        } finally {
          setLoading(false)
        }
      },
      async createTicket(args: Record<string, any>) {
        try {
          setLoading(true)
          const res = await fetch(`/api/tickets?external=true`, {
            method: "POST",
            body: JSON.stringify(args),
            headers: {
              'Authorization': localStorage.getItem("token") ?? ""
            }
          })
          if (!res.ok) {
            throw new Error("error")
          }
          const data = await res.json()
          return data
        } catch (err) {
          return { message: "Failed to create ticket", ticket: null }
        } finally {
          setLoading(false)
        }
      }
    }

    if (functions[fnCall.name]) {
      const res = await functions[fnCall.name](fnCall.arguments)
      const message = {
        role: "function" as const,
        name: fnCall.name,
        content: JSON.stringify(res),
      }
      appendMessage(message);
      streamBotResponse([...messages, message]);
    }
  }

  async function streamBotResponse(messages: Message[]) {
    let fnCall: any
    try {
      setLoading(true)
      const res = await fetch("/api/chat", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          'Authorization': localStorage.getItem("token") ?? "",
        },
        body: JSON.stringify({
          messages,
          parserResponse: state.parserResponse,
        }),
      });
      if (res.ok && res.body) {
        const reader = res.body.getReader();
        let content = "";
        let isStreamStart = true;
        while (true) {
          const { value } = await reader.read();
          let chunk = new TextDecoder("utf-8").decode(value);

          if (chunk.startsWith("__ERROR__")) {
            throw new Error("encountered error streaming chat completion");
          }

          // __EOF__ indicates that the stream has ended
          // __ERROR__ indicates that the stream has errored
          content += chunk.replace("__EOF__", "");

          fnCall = parseFnCall(content)
          if (!fnCall) {
            const message = { role: "assistant", content } as const;
            if (isStreamStart) {
              isStreamStart = false;
              appendMessage(message);
            } else {
              setMessageAt(message, messages.length);
            }
          } else {
            console.log("received function call:", fnCall)
          }
          if (chunk.endsWith("__EOF__")) {
            reader.cancel()
            if (fnCall) {
              invokeFnCall(fnCall.function_call, messages)
            }
            break;
          }
        }
      } else if (!res.ok) {
        throw new Error("failed")
      }
    } catch (err) {
      console.log('err:', err)
      toast.error("Bot failed to answer");
    } finally {
      if (!fnCall) {
        setLoading(false)
      }
    }
  }

  useEffect(() => {
    if (containerRef.current) {
      containerRef.current.scrollIntoView({ behavior: "smooth" });
    }
  }, [state.messages]);

  useEffect(() => {
    login()
  }, [])

  return (
    <>
      <Toaster position="top-right" />
      <main className="h-full flex flex-col">
        <Header />
        <div className="p-8 grid grid-cols-[330px_1fr] flex-1 gap-5 overflow-hidden">
          <UploadDoc
            setUploadStatus={setUploadStatus}
            uploadStatus={state.uploadStatus}
          />
          <Chat
            loading={loading}
            logout={logout}
            username={username}
            messages={state.messages}
            clearChat={clearChat}
            onNewMessage={onNewMessage}
            uploadStatus={state.uploadStatus}
          />
        </div>
        <Login login={login} open={open} close={() => setOpen(false)} />
      </main>
    </>
  );
}
