import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Document to Knowledge Base",
  description: "A simple POC to experiment with OCR and LLMs",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <head>
        <link
          rel="icon"
          href="/icon.svg"
          type="image/svg"
          sizes="any"
        />
      </head>
      <body className={`${inter.className} text-black bg-white`}>
        {children}
      </body>
    </html>
  );
}
