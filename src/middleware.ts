import { NextRequest, NextResponse } from "next/server";

export function middleware(request: NextRequest) {
  const authHeader = request.headers.get("Authorization")
  if (!authHeader) {
    return NextResponse.json({ message: 'unauthorized' }, {
      status: 401
    })
  }
  try {
    // NOTE: this is a hacky way to do stuff. this is just a POC.
    const user = JSON.parse(atob(authHeader))
    const requestHeaders = new Headers(request.headers)
    requestHeaders.set('x-user-id', user.id);
    requestHeaders.set('x-user-name', user.name);
    requestHeaders.set('x-user-job-grade', user.job_grade);
    requestHeaders.set('x-user-location', user.location);
    requestHeaders.set('x-user-gender', user.gender);
    return NextResponse.next({
      request: {
        headers: requestHeaders
      }
    })
  } catch (err) {
    return NextResponse.json({ message: 'unauthorized' }, {
      status: 400
    })
  }
}

export const config = {
  matcher: ['/api/chat', '/api/tickets'],
}
