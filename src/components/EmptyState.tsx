import ComputerIcon from "@/assets/computer.svg";
import Image from "next/image";

type Props = {
  uploadStatus: string;
  hasMessages: boolean;
  username: string
};

export function EmptyState(props: Props) {
  const { uploadStatus, username } = props;
  return (
    <div className="flex-1 grid place-items-center">
      <div className="flex flex-col gap-3 items-center">
        <Image src={ComputerIcon} alt="empty state" />
        <p className="text-[#475569] text-base text-center w-[40ch] flex flex-col gap-1">
          <span>Welcome back, {username}</span>
          <span>
            {uploadStatus === "idle"
              ? `Get started by uploading your document`
              : uploadStatus === "uploading"
              ? `Processing your document, it might take a while`
              : "Document successfully processed. How can I help you today?"}
          </span>
        </p>
      </div>
    </div>
  );
}
