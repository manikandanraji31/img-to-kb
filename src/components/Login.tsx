import { useLoading } from '@/lib/hooks/useLoading'
import { Dialog } from '@headlessui/react'
import toast from 'react-hot-toast'

type Props = {
  open: boolean
  close: () => void
  login: () => void
}

export function Login(props: Props) {
  const [loading, setLoading] = useLoading(false)

  async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault()
    const data = new FormData(e.target as any);
    const payload = {
      username: data.get("username"),
      password: data.get("password"),
    }
    setLoading(true)
    try {
      const res = await fetch("/api/auth/login", {
        method: "POST",
        body: JSON.stringify(payload)
      })
      if (!res.ok) {
        throw new Error("failed to login")
      }
      const { token } = await res.json()
      localStorage.setItem("token", token)
      props.login()
    } catch (err) {
      toast.error("Failed to login")
    } finally {
      setLoading(false)
    }
  }

  return (
    <Dialog open={props.open} onClose={() => { }} className="relative z-50 text-[#374151]">
      <div className="fixed inset-0 flex w-screen items-center justify-center p-4 bg-black/50">
        <Dialog.Panel className="w-full max-w-sm rounded bg-white sheet p-4">
          <form onSubmit={handleSubmit}>
            <div className="mb-6 text-center">
              <h1 className="text-2xl pb-1">Login</h1>
              <p className="text-[#244565]/60 text-sm">You need to be authorized to access the bot</p>
            </div>
            <div className="mb-4">
              <label htmlFor="username" className="pb-1 block">Username</label>
              <input autoComplete="false" name="username" id="username" className="px-2 py-1 border border-[#7090B070] w-full rounded-md focus:outline-none" required />
            </div>
            <div className="mb-4">
              <label htmlFor="password" className="pb-1 block">Password</label>
              <input autoComplete="false" name="password" type="password" id="password" className="px-2 py-1 border border-[#7090B070] w-full rounded-md focus:outline-none" required />
            </div>
            <div className="flex">
              <button disabled={loading} className="ml-auto text-white gradient-bg px-4 py-1 rounded-md disabled:cursor-not-allowed flex gap-2 items-center">
                <span>Submit</span>
                {loading && (
                  <svg
                    className="w-4 h-4 text-white-200 animate-spin fill-pink-600"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 100 101"
                  >
                    <path
                      fill="currentColor"
                      d="M100 50.59c0 27.615-22.386 50.001-50 50.001s-50-22.386-50-50 22.386-50 50-50 50 22.386 50 50zm-90.919 0c0 22.6 18.32 40.92 40.919 40.92 22.599 0 40.919-18.32 40.919-40.92 0-22.598-18.32-40.918-40.919-40.918-22.599 0-40.919 18.32-40.919 40.919z"
                    ></path>
                    <path
                      fill="currentFill"
                      d="M93.968 39.04c2.425-.636 3.894-3.128 3.04-5.486A50 50 0 0041.735 1.279c-2.474.414-3.922 2.919-3.285 5.344.637 2.426 3.12 3.849 5.6 3.484a40.916 40.916 0 0144.131 25.769c.902 2.34 3.361 3.802 5.787 3.165z"
                    ></path>
                  </svg>
                )}
              </button>
            </div>
          </form>
        </Dialog.Panel>
      </div>
    </Dialog>
  )
}
