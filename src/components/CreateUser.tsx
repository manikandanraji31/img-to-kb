import { useEffect, useState } from "react"
import { Dialog } from '@headlessui/react'
import toast, { Toaster } from 'react-hot-toast'
import Select from 'react-select'
import { genders, jobGrades, locations } from "@/lib/data"

type Props = {
  fetchUsers: () => Promise<void>
}

export function CreateUser(props: Props) {
  const [open, setOpen] = useState(false)
  const [loading, setLoading] = useState(false)
  const [user, setUser] = useState({
    name: null,
    password: null,
    jobGrade: null,
    location: null,
    gender: null,
  })

  function updateUser(key: keyof typeof user, value: string) {
    setUser(prev => ({ ...prev, [key]: value }))
  }

  async function createUser(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault()
    setLoading(true)
    try {
      const res = await fetch("/api/users", {
        method: "POST",
        body: JSON.stringify(user)
      })
      if (!res.ok) {
        throw new Error("failed to create new user")
      }
      await props.fetchUsers()
      setOpen(false)
    } catch (err) {
      toast.error("Failed to create new user")
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    if (!open) {
      setUser({
        name: null,
        password: null,
        jobGrade: null,
        location: null,
        gender: null,
      })
    }
  }, [open])

  return (
    <>
      <Toaster position="top-right" />
      <button onClick={() => setOpen(true)} className="gradient-bg px-2.5 py-1 text-white rounded-lg sheet">
        Add User
      </button>
      <Dialog open={open} onClose={() => setOpen(false)} className="relative z-50">
        <div className="fixed inset-0 flex w-screen items-center justify-center p-4 bg-black/50 text-[#374151]">
          <Dialog.Panel className="w-full max-w-lg rounded bg-white sheet p-4">
            <form onSubmit={createUser}>
              <div className="mb-6 text-center">
                <h1 className="text-2xl pb-1">Create User</h1>
              </div>

              <div className="mb-4 flex gap-3">
                <div>
                  <label htmlFor="username" className="pb-1 block">Username</label>
                  <input value={user.name ?? ""} onChange={e => updateUser("name", e.target.value)} autoComplete="false" name="username" id="username" className="px-2 py-1 border border-[#7090B070] w-full rounded-lg focus:outline-none" required />
                </div>
                <div>
                  <label htmlFor="password" className="pb-1 block">Password</label>
                  <input type="password" value={user.password ?? ""} onChange={e => updateUser("password", e.target.value)} autoComplete="false" name="password" id="password" className="px-2 py-1 border border-[#7090B070] w-full rounded-lg focus:outline-none" required />
                </div>
              </div>


              <div className="mb-4">
                <label htmlFor="gender" className="pb-1 block">Gender</label>
                <Select onChange={val => updateUser('gender', val?.value ?? '')} name="gender" options={genders}
                  styles={{
                    control: (baseStyles) => ({
                      ...baseStyles,
                      borderColor: '#7090B070'
                    }),
                  }}

                />
              </div>

              <div className="mb-4 flex gap-3">
                <div className="flex-1">
                  <label htmlFor="job_grade" className="pb-1 block">Job Grade</label>
                  <Select onChange={val => updateUser('jobGrade', val?.value ?? '')} name="job_grade" options={jobGrades}
                    styles={{
                      control: (baseStyles) => ({
                        ...baseStyles,
                        borderColor: '#7090B070'
                      }),
                    }}

                  />
                </div>
                <div className="flex-1">
                  <label htmlFor="location" className="pb-1 block">Location</label>
                  <Select onChange={val => updateUser('location', val?.value ?? '')} name="location" options={locations}
                    styles={{
                      control: (baseStyles) => ({
                        ...baseStyles,
                        borderColor: '#7090B070'
                      }),
                    }}

                  />
                </div>
              </div>
              <div className="flex mt-6">
                <button disabled={loading} className="ml-auto text-white gradient-bg px-4 py-1 rounded-lg disabled:cursor-not-allowed flex gap-2 items-center">
                  <span>Submit</span>
                  {loading && (
                    <svg
                      className="w-4 h-4 text-white-200 animate-spin fill-pink-600"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 100 101"
                    >
                      <path
                        fill="currentColor"
                        d="M100 50.59c0 27.615-22.386 50.001-50 50.001s-50-22.386-50-50 22.386-50 50-50 50 22.386 50 50zm-90.919 0c0 22.6 18.32 40.92 40.919 40.92 22.599 0 40.919-18.32 40.919-40.92 0-22.598-18.32-40.918-40.919-40.918-22.599 0-40.919 18.32-40.919 40.919z"
                      ></path>
                      <path
                        fill="currentFill"
                        d="M93.968 39.04c2.425-.636 3.894-3.128 3.04-5.486A50 50 0 0041.735 1.279c-2.474.414-3.922 2.919-3.285 5.344.637 2.426 3.12 3.849 5.6 3.484a40.916 40.916 0 0144.131 25.769c.902 2.34 3.361 3.802 5.787 3.165z"
                      ></path>
                    </svg>
                  )}
                </button>
              </div>
            </form>
          </Dialog.Panel>
        </div>
      </Dialog>
    </>
  )
}
