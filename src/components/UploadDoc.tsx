"use client";

import { ParserResponse, UploadStatus } from "@/app/page";
import { config } from "@/config";
import { useRef } from "react";
import toast from "react-hot-toast";
import UploadIcon from "@/assets/upload.svg";
import DeleteIcon from "@/assets/delete.svg";
import Image from "next/image";

type Props = {
  uploadStatus: UploadStatus;
  setUploadStatus: (status: UploadStatus, parserResponse: ParserResponse) => void;
};

export function UploadDoc(props: Props) {
  const { setUploadStatus, uploadStatus } = props;
  const inputRef = useRef<HTMLInputElement>(null);

  async function onFilePick(e: React.ChangeEvent<HTMLInputElement>) {
    if (e.target.files) {
      const file = e.target.files[0];
      const formData = new FormData();
      formData.append("file", file);

      if(!config.ALLOWED_FILE_TYPES.includes(file.type)) {
        toast.error("Unsupported file");
        return
      }

      setUploadStatus("uploading", {});
      try {
        const res = await fetch(config.PARSER_API, {
          method: "POST",
          body: formData,
        });
        if (!res.ok) {
          throw new Error("failed to convert document to text");
        }
        const data = await res.json();
        setUploadStatus("uploaded", data.response);
      } catch (err) {
        toast.error("Failed to parse");
        console.error(err);
        setUploadStatus("idle", {});
      }
    }
  }

  return (
    <div className="border border-[7090B0] rounded-xl sheet grid place-items-center text-[14px]">
      {uploadStatus === "idle" && (
        <div className="flex flex-col items-center gap-3">
          <button
            onClick={() => {
              if (inputRef.current) {
                inputRef.current.click();
              }
            }}
            className="h-[46px] w-[46px] grid place-items-center border border-[#7090B070] rounded-lg"
          >
            <Image
              width={18}
              height={18}
              alt="upload document"
              src={UploadIcon}
            />
          </button>
          <p className="text-[#244565] text-center">Upload Document</p>
          <input
            className="hidden"
            ref={inputRef}
            type="file"
            accept={config.ALLOWED_FILE_TYPES.join(",")}
            onChange={onFilePick}
          />
        </div>
      )}

      {uploadStatus === "uploading" && (
        <div className="flex flex-col items-center gap-3">
          {/* I know. This is ugly to look at but for some reason, its not working when I am not inline this. */}
          <svg
            className="w-8 h-8 text-gray-200 animate-spin fill-blue-600"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 100 101"
          >
            <path
              fill="currentColor"
              d="M100 50.59c0 27.615-22.386 50.001-50 50.001s-50-22.386-50-50 22.386-50 50-50 50 22.386 50 50zm-90.919 0c0 22.6 18.32 40.92 40.919 40.92 22.599 0 40.919-18.32 40.919-40.92 0-22.598-18.32-40.918-40.919-40.918-22.599 0-40.919 18.32-40.919 40.919z"
            ></path>
            <path
              fill="currentFill"
              d="M93.968 39.04c2.425-.636 3.894-3.128 3.04-5.486A50 50 0 0041.735 1.279c-2.474.414-3.922 2.919-3.285 5.344.637 2.426 3.12 3.849 5.6 3.484a40.916 40.916 0 0144.131 25.769c.902 2.34 3.361 3.802 5.787 3.165z"
            ></path>
          </svg>
          <p className="text-[#244565]">Processing your document...</p>
        </div>
      )}

      {uploadStatus === "uploaded" && (
        <button
          className="border border-[#7090B070] px-3 py-2 flex gap-2.5 rounded-lg"
          onClick={() => setUploadStatus("idle", {})}
        >
          <Image
            width={16}
            height={16}
            alt="delete document"
            src={DeleteIcon}
          />
          <p className="text-black font-medium">Remove Document</p>
        </button>
      )}
    </div>
  );
}
