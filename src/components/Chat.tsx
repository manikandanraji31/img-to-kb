"use client";

import { Message } from "@/app/page";
import { Messages } from "./Messages";
import { EmptyState } from "./EmptyState";

type Props = {
  uploadStatus: string;
  messages: Message[];
  clearChat: () => void;
  onNewMessage: (value: string) => void;
  username: string
  logout: () => void
  loading: boolean
};

export function Chat(props: Props) {
  const { messages, clearChat, onNewMessage, uploadStatus, username, logout, loading } = props;

  return (
    <div className="border border-[7090B0] rounded-xl sheet flex flex-col text-[14px] overflow-hidden">
      <div className="py-3 px-5 bg-[#F5F5F5] rounded-t-lg flex border-b border-[#7090B033] font-medium min-h-[38px]">
        <div className="flex-1 flex gap-2">
          {messages.length > 0 && (
            <button onClick={clearChat}>
              Clear Chat
            </button>
          )}
          {username && (
            <button onClick={logout} className="ml-auto bg-red-100 text-red-500 px-2 py-1 rounded-lg">Log Out</button>
          )}
        </div>
      </div>

      <div className="flex-1 flex flex-col gap-6 p-4 overflow-auto chat">
        {messages.length ? (
          <Messages messages={messages} />
        ) : (
          <EmptyState
            username={username}
            hasMessages={messages.length > 0}
            uploadStatus={uploadStatus}
          />
        )}
      </div>

      <div className="border border-[#7090B070] rounded-lg min-h-[80px] p-2 m-5 relative">
        {loading && (
          <div className="absolute top-[50%] right-[50%] -translate-x-[50%] -translate-y-[50%] font-italic flex gap-2">
            <svg
              className="w-6 h-6 text-gray-200 animate-spin fill-blue-600"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 100 101"
            >
              <path
                fill="currentColor"
                d="M100 50.59c0 27.615-22.386 50.001-50 50.001s-50-22.386-50-50 22.386-50 50-50 50 22.386 50 50zm-90.919 0c0 22.6 18.32 40.92 40.919 40.92 22.599 0 40.919-18.32 40.919-40.92 0-22.598-18.32-40.918-40.919-40.918-22.599 0-40.919 18.32-40.919 40.919z"
              ></path>
              <path
                fill="currentFill"
                d="M93.968 39.04c2.425-.636 3.894-3.128 3.04-5.486A50 50 0 0041.735 1.279c-2.474.414-3.922 2.919-3.285 5.344.637 2.426 3.12 3.849 5.6 3.484a40.916 40.916 0 0144.131 25.769c.902 2.34 3.361 3.802 5.787 3.165z"
              ></path>
            </svg>
          </div>
        )}

        <textarea
          disabled={uploadStatus !== "uploaded" || loading}
          onKeyDown={(e) => {
            const value = e.currentTarget.value.trim();
            if (e.key === "Enter") {
              e.preventDefault();
              if (value) {
                onNewMessage(value);
                e.currentTarget.value = "";
              }
            }
          }}
          className="disabled:cursor-not-allowed disabled:bg-white focus:outline-none h-full w-full"
          placeholder="Type here..."
        />
      </div>
    </div>
  );
}
