import { Message } from "@/app/page";
import { cn } from "@/lib/utils";
import { marked } from "marked";
import { useEffect, useRef } from "react";

type Props = {
  messages: Message[];
};

export function Messages(props: Props) {
  const messagesEndRef = useRef<HTMLDivElement>(null);
  const { messages } = props;

  useEffect(() => {
    if (messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
    }
  }, [messages]);

  return (
    <div className="flex-1 flex flex-col gap-6 p-4 overflow-auto chat">
      {messages.filter(message => message.role !== 'function').map((message, i) => {
        return (
          <article
            key={i}
            className={cn(
              "prose text-[14px] leading-[22px] text-left border border-[#7090B033] chat-bubble py-2.5 pl-2.5 max-w-[525px] bg-[#7090B010] pr-[44px]",
              {
                "self-end pl-[44px] pr-2.5 bg-white text-right":
                  message.role === "user",
              }
            )}
            dangerouslySetInnerHTML={{ __html: marked.parse(message.content) }}
            data-role={message.role}
          />
        );
      })}
      <div ref={messagesEndRef} />
    </div>
  );
}
