import Image from "next/image";
import Logo from "@/assets/logo.svg";

export function Header() {
  return (
    <header className="gradient-bg p-4 relative">
      <Image className="opacity-0" width={90} height={51} alt="logo" src={Logo} />
      <p className="text-[28px] absolute top-0 left-[50%] top-5 -translate-x-[50%] text-white">
        Document to Knowledge Base
      </p>
    </header>
  );
}
